//Cathy Tham, 1944919

package lab06;
import java.util.Random;
import java.util.Scanner;

public class RpsGame {
	public static void main(String[] args) {
		RpsGame game=new RpsGame ();
		System.out.println("-----------------------------ROCK PAPER SCISSORS GAME!-----------------------------");
		System.out.println("Please choose your symbol:");
		System.out.println("Rock, Paper or Scissors");
		Scanner reader = new Scanner(System.in);
		String playerChoice = reader.next();
		
		System.out.println(game.playRound(playerChoice));
	}

	private int wins;
	private int ties; 
	private int losses;
	
	private Random rand;
	
	//constructor to set fields to 0
	public RpsGame() {
		wins = 0;
		ties = 0;
		losses = 0;
		rand= new Random();
	}
	
	//get methods
	public int getWins() {
		return wins;
	}
	public int getTies() {
		return ties;
	}
	public int getLosses() {
		return losses;
	}
	
	public String playRound(String playerChoice) {
		int compChoice = rand.nextInt(3);
		//System.out.println(compChoice);
		
		String compSymbol;
		
		String winner;
		
		if (compChoice == 0) {
			compSymbol = "Rock";
		} 
		
		else if (compChoice == 1) {
			compSymbol = "Scissors";
		}
		
		else {
			compSymbol = "Paper";
		}
		
		//System.out.println(compSymbol);
		
		if (playerChoice.equals("Rock") && compSymbol.equals("Scissors") ||
		    playerChoice.equals("Paper") && compSymbol.equals("Rock") ||
		    playerChoice.equals("Scissors") && compSymbol.equals("Paper")) {
			
			wins++;
			winner = "Player";
			
		} 
		
		else if (playerChoice.equals("Scissors") && compSymbol.equals("Rock") ||
				playerChoice.equals("Paper") && compSymbol.equals("Scissors") ||
				playerChoice.equals("Rock") && compSymbol.equals("Paper")) {
			losses++;
			winner = "Computer";
		}
		
		else {
			ties++;
			winner = "No one";
		}
		return ("Computer plays " + compSymbol + " and " + winner + " won!");
	}

}