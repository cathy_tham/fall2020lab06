//Cathy Tham, 1944919

package lab06;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;

public class RpsChoice implements EventHandler<ActionEvent>{
	private TextField message;
	private TextField wins;
	private TextField losses;
	private TextField ties;
	private String playerChoice;
	private RpsGame game;
	
	//constructor
	public RpsChoice(TextField message, TextField wins, TextField losses, TextField ties, String playerChoice, RpsGame game) {
		this.message = message;
		this.wins = wins;
		this.losses = losses;
		this.ties = ties;
		this.playerChoice = playerChoice;
		this.game = game;
	}

	@Override
	public void handle(ActionEvent e) {
		String resultMessage = game.playRound(playerChoice);
		
		message.setText(resultMessage);
		wins.setText("Wins: " + game.getWins());
		losses.setText("Losses: " + game.getLosses());
		ties.setText("Ties: " + game.getTies());
		
	}
}
