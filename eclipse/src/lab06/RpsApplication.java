//Cathy Tham, 1944919

package lab06;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsApplication extends Application {	
	private RpsGame game =new RpsGame();
	
	public void start(Stage stage) {
		Group root = new Group(); 
		
		//each button added to HBOx buttons 
		HBox buttons = new HBox();
		Button rockButton= new Button("rock");
		Button scissorsButton= new Button("scissors");
		Button paperButton= new Button("paper");
		
		buttons.getChildren().addAll(rockButton,scissorsButton,paperButton);
		
		//HBox TextField  
		HBox textField = new HBox();
		TextField welcome = new TextField("Welcome!");
		//set width of message text field
		welcome.setPrefWidth(200);
		
		//TextField objects
		TextField wins = new TextField("Wins: 0");
		TextField losses = new TextField("Losses: 0");
		TextField ties = new TextField("Ties: 0");
		textField.getChildren().addAll(welcome,wins,losses,ties);
		
		//TextFields and buttons added to Vbox
		VBox overall = new VBox();
		//add multiple nodes to Vbox
		overall.getChildren().addAll(buttons,textField);
		
		//Vbox added to the root
		root.getChildren().add(overall);
	
		RpsChoice rock =new RpsChoice( welcome, wins, losses, ties, "Rock", game);
		rockButton.setOnAction(rock);
		RpsChoice scissors =new RpsChoice(welcome, wins, losses, ties, "Scissors", game);
		scissorsButton.setOnAction(scissors);
		RpsChoice paper =new RpsChoice(welcome, wins, losses, ties, "Paper", game);
		paperButton.setOnAction(paper);
		
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);
		
		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 	
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    



